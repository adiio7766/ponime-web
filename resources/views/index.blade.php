<!DOCTYPE html>
<html lang="en">

<a ></a>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ponime</title>

    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <!-- Favicon icon -->
    <link rel="icon" type="{{asset('image/png')}}" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<body>

    <div class="preloader"></div>

    <div class="main-wrapper">
        <!-- header wrapper -->
        <div class="header-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 navbar p-0">
                        <a href="#" class="logo">PONIME</a>
                         <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav nav-menu float-none text-center">
                                <li class="nav-item"><a class="nav-link" href="season.html">On-Going</a></li>
                                <li class="nav-item"><a class="nav-link" href="single.html">Genres</a></li>
                                <li class="nav-item"><a class="nav-link" href="search.html">Report-Link Rusak</a></li>
                             
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="search-div">
                            <input type="text" placeholder="Cari Anime">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header wrapper -->
        <!-- banenr wrapper -->
        <div class="banner-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="banner-slider owl-carousel owl-theme">
                            @foreach($data['bydate'] as $a)
                            <div class="owl-items">
                                <div class="banner-wrap justify-content-between align-items-center">
                                    <div class="left-wrap">                                   
                                        <span class="rnd">Skor MAL {{ $a->skor }}</span>
                                        <h2>{{ $a->nama_anime }}</h2>
                                        
                                        <span class="tag"><b>{{ $a->jenis }}</b></span>

                                        <span class="tag"><b>{{ $a->status }}</b></span>

                                        <span class="tag">{{ $a->tahun }}</span>
                                        <span class="tag">Jumlah EPS : {{ $a->jumlah_eps }}
                                        </span><p>{{ $a->deskripsi_kecil }}</p>
                                        <a href="/tonton/{{ $a->slug }}" class="btn btn-lg btn-video"><img src="images/play.png" alt="icn">Watch</a>
                                    </div>
                                    <div class="right-wrap" style="background-image: url({{ $a->path_img }});"></div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banenr wrapper -->
        <!-- slider wrapper -->
        <div class="slide-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-left mb-4 mt-4">
                        <h2>On-Going Anime</h2>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="slide-slider owl-carousel owl-theme">
                            @foreach ($data['ongoing'] as $ongoing)
                            <div class="owl-items">
                                <a class="slide-one" href="/anime/{{ $ongoing->slug }}">
                                    <div class="slide-image"><img src="{{$ongoing->path_img}}" alt="image" height="50%"></div>
                                    <div class="slide-content">
                                        <h2>{{$ongoing->nama_anime}} </h2>
                                        <p>{{$ongoing->deskripsi_kecil}}</p>
                                        <span class="tag">{{$ongoing->status}}</span>
                                        <span class="tag">{{$ongoing->tahun}}</span>
                                        <span class="tag"><b>{{$ongoing->jumlah_eps}}</b></span>
                                       
                                    </div>
                                </a>
                            </div>
                            @endforeach
                       
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider wrapper -->
        <!-- slider wrapper -->
        <div class="slide-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-left mb-4 mt-1">
                        <h2>Complete</h2>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="slide-slider owl-carousel owl-theme">
                               @foreach ($data['complete'] as $complete)
                            <div class="owl-items">
                                <a class="slide-one" href="/anime/{{ $complete->slug }}">
                                    <div class="slide-image"><img src="{{$complete->path_img}}" alt="image" height="50%"></div>
                                    <div class="slide-content">
                                        <h2>{{$complete->nama_anime}} </h2>
                                        <p>{{$complete->deskripsi_kecil}}</p>
                                        <span class="tag">{{$complete->status}}</span>
                                        <span class="tag">{{$complete->tahun}}</span>
                                        <span class="tag"><b>{{$complete->jumlah_eps}}</b></span>
                                       
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider wrapper -->
    
       
        <!-- slider wrapper -->
        <div class="slide-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-left mb-4 mt-1">
                        <h2>Upload-an Terbaru</h2>
                    </div>
                     
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="slide-slider owl-carousel owl-theme">
                            @foreach ($data['bydate'] as $byid)
                            <div class="owl-items">
                                <a class="slide-one" href="/anime/season.html">
                                    <div class="slide-image"><img src="{{$byid->path_img}}" alt="image" height="50%"></div>
                                    <div class="slide-content">
                                        <h2>{{$byid->nama_anime}} </h2>
                                        <p>{{$byid->deskripsi_kecil}}</p>
                                        <span class="tag">{{$byid->status}}</span>
                                        <span class="tag">{{$byid->tahun}}</span>
                                        <span class="tag"><b>{{$byid->jumlah_eps}}</b></span>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider wrapper -->
        <!-- footer wrapper -->
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-left">
                        <h4 class="mb-4">Kami Adalah Website Fan-share Anime.Download Juga Aplikasi Android Nya </h4>
                    </div>
                    <div class="col-sm-6 text-left">
                        <img src="images/icon-21.png" alt="icon" class="icon-img"> 
                    </div>
                   
                 
                    <div class="col-sm-12 lower-footer"></div>
                    <div class="col-sm-6">
                        <p class="copyright-text">© 2020 copyright. All rights reserved.</p>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- footer wrapper -->

    </div>

    

    <script src="js/plugin.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/scripts.js"></script>
</body>


</html>