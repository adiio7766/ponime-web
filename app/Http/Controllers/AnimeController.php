<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AnimeController extends Controller
{
    
    public function showhome()
    {
    	// mengambil data dari table anime
    	
    	$data['bydate']=DB::table('tb_anime')->orderByRaw('id_anime')->take(5)->get();
		
		$data['ongoing']=DB::table('tb_anime')->where('status','LIKE','%On-Going%')->get();
		
		$data['complete']=DB::table('tb_anime')->where('status','LIKE','%Complete%')->get();
		
		//Mengirinm data ke view index 
		
		return view('index',compact('data'));
    }

    // show anime di view anime berdasarkan slug yang dipilih
	public function showanime($slug)
	{	
		//Mengambil Data anime berdasarkan slug
		$data['anime']=DB::table('tb_anime')->where('slug','LIKE',$slug)->get();
		
		//cek apakah slug ada
		if ($data['anime']->isNotEmpty()) { 
			
			$idanime=0;
			//menaruh tb_anime.id_anime ke variable idanime 
			foreach ($data['anime'] as $a) {
			$idanime=$a->id_anime;
			}

			//mengambil data episode berdasarkan variable idanime

			$data['episode']=DB::table('tb_episode')->where('id_anime',$idanime)->get();

			$data['random']=DB::table('tb_anime')->orderByRaw('id_anime')->take(5)->get();
		
			// passing data anime,episode
			//return view('anime',['data' => $data]);

			return view('anime', compact('data'));
		}
		//jika tidak menemukan slug
		else {
		  return abort(404);
		}
	}
	
	public function watch($slug)
    {
    	//koding lom jadi
		
		return view('watch');
    }

}
