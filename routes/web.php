<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','App\Http\Controllers\AnimeController@showhome');
Route::get('/index','App\Http\Controllers\AnimeController@showhome');

Route::get('/anime/{id}','App\Http\Controllers\AnimeController@showanime');
Route::get('/watch/{slug}','App\Http\Controllers\AnimeController@watch');
Route::middleware(['auth:sanctum', 'verified'])->get('admin', function () {
    return view('/admin/dashboard');
    
})->name('dashboard');
