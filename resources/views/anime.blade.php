<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ponime</title>

    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <!-- Favicon icon -->
    <link rel="icon" type="{{asset('image/png')}}" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<div class="preloader"></div>

    <div class="main-wrapper">
        <!-- header wrapper -->
        <div class="header-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 navbar p-0">
                        <a href="http://127.0.0.1:8000/" class="logo">PONIME</a>
                       <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav nav-menu float-none text-center">
                                <li class="nav-item"><a class="nav-link" href="season.html">On-Going</a></li>
                                <li class="nav-item"><a class="nav-link" href="single.html">Genres</a></li>
                                <li class="nav-item"><a class="nav-link" href="search.html">Report-Link Rusak</a></li>
                             
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="search-div">
                            <input type="text" placeholder="Cari Anime">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- header wrapper -->
        <!-- banenr wrapper -->
        <div class="banner-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="banner-wrap justify-content-between align-items-center">
                            <div class="left-wrap">
                                @foreach($data['anime'] as $anime)
                                <span class="rnd">MAL:{{$anime->skor}}</span>
                                <h2>{{$anime->nama_anime}}</h2>
                                <span class="tag">{{$anime->status}}</span>
                                <span class="tag">{{$anime->tahun}}</span>
                                <span class="tag"><b>Jumlah Eps : {{$anime->jumlah_eps}}</b></span>
                                @endforeach
                            </div>
                            <div class="right-wrap" style="background-image: url({{$anime->path_img}});"></div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="slide-wrapper">
                        <div class="slide-slider owl-carousel owl-theme">
                            @foreach($data['episode'] as $episode)
                            <div class="owl-items">
                                <a class="slide-one slide-two slide-three" href="/watch/{{$episode->slug}}">
                                    <div class="slide-image" style="background-image: url({{$anime->path_img}});"></div>
                                    <div class="slide-content">
                                        <h2>{{$episode->nama_episode}} </h2>
                                        
                                    </div>
                                </a>
                            </div>
                            @endforeach
                            
                        </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banenr wrapper -->

        <!-- crew wrapper -->
        <div class="slide-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-left mb-4 mt-4">
                        <h2>Info Anime</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        
                        <p>
                           {{$anime->deskripsi}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        

        <!-- slider wrapper -->
        <div class="slide-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 text-left mb-4 mt-4">
                        <h2>Uploadan Terbaru</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="slide-slider owl-carousel owl-theme">
                            @foreach($data['random'] as $random)
                            <div class="owl-items">
                                <a class="slide-one" href="{{$random->slug}}">
                                    <div class="slide-image"><img src="{{$random->path_img}}" alt="image"></div>
                                    <div class="slide-content">
                                        <h2>{{$random->nama_anime}} </h2>
                                        <p>{{$random->deskripsi_kecil}}.</p>
                                        
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- slider wrapper -->
         
        <!-- footer wrapper -->
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-left">
                        <h4 class="mb-4">Kami Adalah Website Fan-share Anime.Download Juga Aplikasi Android Nya </h4>
                    </div>
                    <div class="col-sm-6 text-left">
                        <img src="images/icon-21.png" alt="icon" class="icon-img"> 
                    </div>
                   
                 
                    <div class="col-sm-12 lower-footer"></div>
                    <div class="col-sm-6">
                        <p class="copyright-text">© 2020 copyright. All rights reserved.</p>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- footer wrapper -->

    </div>

    


    <script src="{{asset('js/plugin.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    
</body>


<!-- Mirrored from gloveswork.in/vstream/season.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 19 Sep 2020 07:42:12 GMT -->
</html>