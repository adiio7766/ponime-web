<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Watch</title>

    <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Custom Stylesheet -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/video-player.css')}}">

  
</head>

<body class="video-player">

    <div class="preloader"></div>

    

    <div class='player-container'>
        <a href="{{ url()->previous() }}" class="close-video-player"><i class="ti-close"></i></a>
       
        <iframe src="https://content.jwplatform.com/players/QI9oRv08-aBCdE12G.html" width="100%" height="100%" frameborder="0" scrolling="auto" allowfullscreen style="position:absolute;"></iframe>

    </div>
    </div>
    
    <script src="{{asset('js/plugin.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/scripts.js')}}"></script>
    <script src="{{asset('js/video-player.js')}}"></script>    
</body>



</html>